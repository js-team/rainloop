// gulp-less@4.0.0

var path           = require('path');
//var accord         = require('accord');
var through2       = require('through2');
//var replaceExt     = require('replace-ext');
//var assign         = require('object-assign');
//var applySourceMap = require('vinyl-sourcemaps-apply');
var PluginError    = require('plugin-error');

var less           = require('less');

module.exports = function (options) {
  // Mixes in default options.
  var opts = Object.assign({}, {
    compress: false,
    paths: []
  }, options);

  return through2.obj(function(file, enc, cb) {
    if (file.isNull()) {
      return cb(null, file);
    }

    if (file.isStream()) {
      return cb(new PluginError('gulp-less', 'Streaming not supported'));
    }

    var str = file.contents.toString();

    // Injects the path of the current file
    opts.filename = file.path;

    // Bootstrap source maps
    if (file.sourceMap) {
      opts.sourcemap = true;
    }

    less.render(str, opts).then(function(res) {
      file.contents = new Buffer(res.css);
//      file.path = replaceExt(file.path, '.css');
      file.path = file.path.substring(0, file.path.lastIndexOf('.')) + '.css';
/*
      if (res.sourcemap) {
        res.sourcemap.file = file.relative;
        res.sourcemap.sources = res.sourcemap.sources.map(function (source) {
          return path.relative(file.base, source);
        });

        applySourceMap(file, res.sourcemap);
      }
*/
      cb(null, file);
    }).catch(function(err) {
      // Convert the keys so PluginError can read them
      err.lineNumber = err.line;
      err.fileName = err.filename;

      // Add a better error message
      err.message = err.message + ' in file ' + err.fileName + ' line no. ' + err.lineNumber;
      return cb(new PluginError('gulp-less', err));
    });
  });
};
